## Este script prepara todo lo necesario para poder ejecutar un cluster en AWS ##
## Debera correrse una UNICA vez, y debera correrse de nuevo ante cada cambio en los archivos que este script sube ##

### WORDCOUNT OFICIAL HADOOP - INPUT pg132.txt

# Archivos necesarios para correr el wordcount original de hadoop
aws s3 cp $HADOOP_HOME/share/hadoop/mapreduce/hadoop-mapreduce-examples-2.4.0.jar s3://bucket-jose/wordcount/hadoop-mapreduce-examples-2.4.0.jar
aws s3 cp $HADOOP_HOME/pg132.txt s3://bucket-jose/wordcount/input/pg132.txt

# Muevo este archivo, para crear la carpeta output, no encontre como crearla de otra forma
aws s3 cp $HADOOP_HOME/input.txt s3://bucket-jose/wordcount/output/input.txt

### FIN WORDCOUNT OFICIAL HADOOP

## GIRAPH ##
aws s3 cp /usr/local/hadoop/aws/zookeeper-aws.tar.gz s3://bucket-jose/giraph/zookeeper.tar.gz

# GIRAPH Y ZOOKEEPER Y SSH
aws s3 cp $HADOOP_HOME/aws/configurar-giraph-y-zookeeper-en-aws-bootstrap-action.sh s3://bucket-jose/configurar-giraph-y-zookeeper-en-aws-bootstrap-action.sh
aws s3 cp $HADOOP_HOME/aws/conf-ssh-para-zookeeper-master-via-bash.sh s3://bucket-jose/conf-ssh-para-zookeeper-master-via-bash.sh
aws s3 cp $HADOOP_HOME/aws/conf-ssh-para-zookeeper-slave-via-bash.sh s3://bucket-jose/conf-ssh-para-zookeeper-slave-via-bash.sh
aws s3 cp $HADOOP_HOME/aws/agregar-certificados-ssh-de-otros-nodos.sh  s3://bucket-jose/agregar-certificados-ssh-de-otros-nodos.sh
aws s3 cp $HADOOP_HOME/aws/configurar-archivo-conf-zookeeper.sh s3://bucket-jose/configurar-archivo-conf-zookeeper.sh
aws s3 cp $HADOOP_HOME/aws/iniciar-cluster-desde-master.sh s3://bucket-jose/iniciar-cluster-desde-master.sh
aws s3 cp $HADOOP_HOME/aws/iniciar-cluster-desde-master-wikipedia.sh s3://bucket-jose/iniciar-cluster-desde-master-wikipedia.sh
aws s3 cp $HADOOP_HOME/aws/iniciar-zookeeper.sh s3://bucket-jose/iniciar_zookeeper.sh

# SUBO EL JAR PARA PODER CORRER MIS PROGRAMAS EN AWS
aws s3 cp /home/hduser/Desktop/Tesina/repositorio_git_tesina/generacion_entrada_giraph/procesamiento_grafo_wikiqoutes/target/procesamiento_grafo_wikiquote-0.0.3.jar s3://bucket-jose/giraph/procesamiento-grafo-0.0.3.jar
aws s3 cp /home/hduser/Desktop/Tesina/repositorio_git_lectura_dump/lectura_dump_wikiquote/generacion_grafo_wikiquote/target/generacion_grafo_wikiquote-0.0.2-SNAPSHOT.jar s3://bucket-jose/giraph/generacion_grafo_wikiquote-0.0.2-SNAPSHOT.jar

#Este archivo se debe cargar unicamente para casos de pruebas particulares, dado que generalmente habra multiples archivos de vertices
#aws s3 cp /usr/local/hadoop/probando_generacion_automatica_vertices s3://bucket-jose/giraph/datos/vertices.txt
