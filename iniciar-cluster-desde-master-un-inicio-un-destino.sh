#!/bin/bash
if [ ! -f /tmp/keypair-amazon.pem ];
then
    echo "No se encontro la clave keypair-amazon.pem"
else
    	#Existe la clave,  se inicia la configuracion del cluster
        echo "Archivo keypairuseastjose.pem encontrado en directorio /tmp, iniciando configuracion cluster"

	if [ "$#" -ne 1 ]; then
        	echo "Parametros: directorio-para-guardar-logs"
	else
		aws s3 cp s3://bucket-jose/giraphlogs/"$1"/vertices-$(cat /mnt/var/lib/info/job-flow.json | grep jobFlowId | cut -f2 -d: | cut -f2 -d'"').txt /tmp/vertices.txt
		aws s3 cp s3://bucket-jose/giraph/datos/grafo-wikiquotes.txt /tmp/grafo-wikiquotes.txt
	
	
		if [ ! -f /tmp/vertices.txt ];
		then
			echo "ERROR:                     No se encontro el archivo de vertices en /tmp/vertices.txt"
		else
		
	
        		echo "Verificando que todos los procesos de hadoop esten inicializados"
        		echo "TODO:Hacer, necesito hacer un wait sobre hdfs dfsadmin -report... "
		
        		#Configurando HDFS
        		echo "Creando carpetas en HDFS"
        		/home/hadoop/bin/hdfs dfs -mkdir /user/
        		/home/hadoop/bin/hdfs dfs -mkdir /user/hduser
        		/home/hadoop/bin/hdfs dfs -mkdir /user/hduser/input
        		/home/hadoop/bin/hdfs dfs -mkdir /user/hduser/output
		
        		echo "Descargando scripts"
        		aws s3 cp s3://bucket-jose/configurar-giraph-y-zookeeper-en-aws-bootstrap-action-version-un-inicio-un-destino.sh /tmp/configurar-giraph-y-zookeeper-en-aws-bootstrap-action.sh
        		aws s3 cp s3://bucket-jose/conf-ssh-para-zookeeper-master-via-bash.sh /tmp/conf-ssh-para-zookeeper-master-via-bash.sh
        		aws s3 cp s3://bucket-jose/conf-ssh-para-zookeeper-slave-via-bash.sh /tmp/conf-ssh-para-zookeeper-slave-via-bash.sh
        		aws s3 cp s3://bucket-jose/agregar-certificados-ssh-de-otros-nodos.sh /tmp/agregar-certificados-ssh-de-otros-nodos.sh
        		aws s3 cp s3://bucket-jose/configurar-archivo-conf-zookeeper.sh /tmp/configurar-archivo-conf-zookeeper.sh
			aws s3 cp s3://bucket-jose/iniciar_zookeeper.sh /tmp/iniciar-zookeeper.sh
        		chmod 750 /tmp/configurar-giraph-y-zookeeper-en-aws-bootstrap-action.sh
        		chmod 750 /tmp/conf-ssh-para-zookeeper-master-via-bash.sh
        		chmod 750 /tmp/conf-ssh-para-zookeeper-slave-via-bash.sh
        		chmod 750 /tmp/agregar-certificados-ssh-de-otros-nodos.sh
        		chmod 750 /tmp/configurar-archivo-conf-zookeeper.sh
			chmod 750 /tmp/iniciar-zookeeper.sh
		
        		ip_master="$(sudo -s ifconfig | grep -A 1 'eth0' | tail -1 | cut -d ':' -f 2 | cut -d ' ' -f 1)"
        		echo Ip del master: "$ip_master"
		
        		ips="$(/home/hadoop/bin/hdfs dfsadmin -report | grep Name | cut -f2 -d: | cut -f2 -d' ')"
        		#ips="$(/usr/local/hadoop/bin/hdfs dfsadmin -report | grep Name | cut -f2 -d: | cut -f2 -d' ')"
        		echo Ip de los slaves:
        		echo  "${ips}"
		
        		x=$(echo -e "$ips")
		
		
			hdfs dfs -copyFromLocal /tmp/grafo-wikiquotes.txt hdfs:///user/hduser/input/grafo-wikiquotes.txt
        		/tmp/conf-ssh-para-zookeeper-master-via-bash.sh "$ip_master"
		
        		#Configuracion ZOOKEEPER y detalles faltantes de SSH
        		num=1
			/tmp/configurar-giraph-y-zookeeper-en-aws-bootstrap-action.sh "$1"
        		/tmp/configurar-archivo-conf-zookeeper.sh "$ip_master" "$ip_master" "$num"
        		while read ip_slave
			do
                		echo Configurando ZOOKEEPER en slave con IP: "$ip_slave"
               			num=$((num+1))
                		ssh -i /tmp/keypair-amazon.pem hadoop@"$ip_slave" 'bash -s' < /tmp/configurar-giraph-y-zookeeper-en-aws-bootstrap-action.sh "$1"
               			ssh -i /tmp/keypair-amazon.pem hadoop@"$ip_slave" 'bash -s' < /tmp/configurar-archivo-conf-zookeeper.sh "$ip_master" "$ip_slave" "$num"
    			done <<< "$x"
		
        		#Inicializacion de servidores ZOOKEEPER
        		/tmp/iniciar-zookeeper.sh
        		while read ip_slave
        		do
                		echo Iniciando ZOOKEEPER en slave con IP: "$ip_slave"
                		ssh -i /tmp/keypair-amazon.pem hadoop@"$ip_slave" 'bash -s' < /tmp/iniciar-zookeeper.sh
        		done <<< "$x"
			
		fi
	fi
fi
