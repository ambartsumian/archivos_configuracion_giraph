#!/bin/bash
if [ "$#" -ne 2 ]; then
        echo "Parametros: cluster-id directorio-para-guardar-resultados-por-cluster"
else
	if aws s3 ls bucket-jose/giraphlogs/ | grep "$2"; then
		ssh -A  hadoop@$(aws emr describe-cluster --region us-east-1 --cluster-id "$1" | grep MasterPublicDnsName | cut -d'"' -f4) -i ~/.ssh/keypairuseastjose.pem 'bash -s' < $HADOOP_HOME/aws/cluster-de-clusters/guardar-logs-remoto.sh $2
	else
		echo "ERROR:       NO existe el directorio en el bucket, proceda a crearlo para poder guardar los logs alli. Debería usar el mismo directorio que uso para crear el cluster de clusters..."
	fi
fi


