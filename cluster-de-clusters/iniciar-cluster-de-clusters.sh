#!/bin/bash
sudo ntpdate ntp.ubuntu.com
if [ "$#" -ne 4 ]; then
        echo "Parametros: cantidad_de_clusters directorio-para-guardar-resultados-en-S3 nombre-del-cluster slaves_por_cluster"
else
	if [ ! -f /tmp/vertices.txt ]; then
		echo "No existe el archivo vertices.txt, desde donde se obtendran los pares de vertices para el procesamiento"
	else
		echo "Iniciando proceso de creación de multiples clusters..."
		
		echo "Chequeando contra s3://bucket-jose/giraphlogs/ si existe el directorio donde se guardaran los logs"
		if aws s3 ls bucket-jose/giraphlogs/ | grep "$2"; then
			echo "El directorio especificado como parametro existe en el bucket, se procedera a crear los clusters"
			echo "..."

			#Definimos la cantidad de slaves que tendra cada cluster
			slaves="$4"

			#Lista de archivos de vertices de una ejecucion anterior del script, se borran ya que sern regenerados
                        archivos="$(find . -name 'vertices*' | cut -f2 -d. | cut -f2 -d'/')"
                        while read arch
                        do
				if [ "$arch" != '' ];then
					echo "Borrando archivo: " "$arch"
					rm "$arch"
				fi
                        done <<< "$archivos"

			#nombre del archivo de ids de clusters, es un archivo único por cada cluster (el usuario no debería repetir el nombre)
			nombre_archivo_ids_clusters="$3"-clusters-id.txt
			if [ ! -f "$nombre_archivo_ids_clusters" ]; then
				printf "" > $nombre_archivo_ids_clusters
			fi

			#cantidad de pares de vertices del archivo /tmp/vertices.txt
	                cantparesvertices="$(wc -l /tmp/vertices.txt | cut -f1 -d/ | cut -f1 -d' ')"

			echo "Partiendo el archivo de pares de vertices, de modo de procesarlo con $1 clusters"
			#Dividimos la cantidad de pares  de vertices por la cantidad de clusters, y asi sabemos en cuantos archivos dividir el archivo /tmp/vertices.txt
			if [ $(($cantparesvertices%2)) -eq 1 ];then
				#Si el numero es impar, le sumamos uno, de esta forma evitamos crear un cluster para un solo par de vertices...
				cantparesvertices=$(($cantparesvertices+1))
			fi
			split -l $(($cantparesvertices/$1)) /tmp/vertices.txt vertices

			#Lista de archivos de vertices, cada uno sera enviado a un cluster distinto
	                archivos="$(find . -name 'vertices*' | cut -f2 -d. | cut -f2 -d'/')"

			#Comando para crear un cluster en forma automatica, nos devuelve el ID para poder manipular el cluster en un futuro
			comandclusterid='aws emr create-cluster --name "$3" --use-default-roles --ec2-attributes KeyName=keyPairAmazonJose --ami-version 3.11.0 --log-uri s3://bucket-jose/giraphlogs/"$2" --instance-groups InstanceGroupType=MASTER,InstanceCount=1,InstanceType=m3.xlarge InstanceGroupType=CORE,InstanceCount="$4",InstanceType=m3.xlarge --bootstrap-action Path=s3://elasticmapreduce/bootstrap-actions/configure-hadoop,Name="Configuracion parametros memoria HADOOP",Args=["-m","mapreduce.map.memory.mb=5700","-m","mapreduce.reduce.memory.mb=4096","-m","mapreduce.map.java.opts=-Xmx4320","-m","mapreduce.reduce.java.opts=-Xmx3072"]'

			#Guardamos los archivos que contienen pares de vertices
			nombres_de_archivos=($archivos)

			#Nos aseguramos que haya tantos archivos como clusters requeridos, para no crear clusters de mas o de menos por error
			if [ ${#nombres_de_archivos[@]} -eq "$1" ];then
				echo "La cantidad de archivos de pares de vertices, coincide con la cantidad de cluster requeridos"
				echo "..."
				
				if [ "$(cat $nombre_archivo_ids_clusters | grep j-)" != "" ];then
					echo "Los clusters fueron creados previamente, no seran creados de nuevo..."
				else
					echo "Creando clusters..."

					#Leemos cada uno de los archivos que forman parte del archivo de "pares de vertices" original
			                while read arch
		        	        do
						#Creamos el cluster que procesar a "$arch"
		                 	        eval "$comandclusterid" | grep "\"ClusterId\":" | cut -f2 -d: | cut -f2 -d'"' >> $nombre_archivo_ids_clusters
				                printf "\n" >> $nombre_archivo_ids_clusters
		
			                done <<< "$archivos"
				fi

				echo "Iniciando configuracion interna de clusters"
				numero_de_cluster=0
				#Leemos cada uno de los cluster-id, correspondientes a los clusters generados
				for idcluster in $(sed '/^$/d' $nombre_archivo_ids_clusters);
                	        do
					echo "Cluster id: " "$idcluster"
	                             	#Creamos el cluster que procesar a "$arch"
                        	        echo "Posicion \"$numero_de_cluster\" de archivos:" ${nombres_de_archivos[$numero_de_cluster]}
					#Copiamos a S3 el archivo de pares de vertices de "$arch"
					aws s3 cp ${nombres_de_archivos[$numero_de_cluster]} s3://bucket-jose/giraphlogs/"$2"/vertices-"$idcluster".txt
					numero_de_cluster=$(($numero_de_cluster+1))
					
        	                done

				#Como los MasterPublicDnsName tardan en aparecer, hacemos que el script se detenga por algunos segundos
				echo "Detencion temporal del script, para que aparezcan los MasterPublicDnsName"
				sleep 180
			
				#Obtenemos el MasterPublicDNSName de cada cluster
				numero_de_cluster=0
                                for idcluster in $(sed '/^$/d' $nombre_archivo_ids_clusters);
                                do

					echo "Esperando por el MasterPublicDnsName del cluster $idcluster"
					until [ "$(aws emr describe-cluster --cluster-id $idcluster | grep MasterPublicDnsName | cut -d\" -f4 | wc -l)" -eq 1 ];
					do
						echo "."
						sleep 10
					done
					echo "Copiando certificado .pem al cluster"
					scp -i ~/.ssh/keypair-amazon.pem -o StrictHostKeyChecking=no ~/.ssh/keypair-amazon.pem hadoop@"$(aws emr describe-cluster --cluster-id $idcluster | grep MasterPublicDnsName | cut -d\" -f4)":/tmp
                                done

				#Como los MasterPublicDnsName tardan en aparecer, hacemos que el script se detenga por algunos segundos 
                                echo "Detencion temporal del script, para que aparezcan los datanodes"
                                sleep 120


				# Esperamos que esten activos los datanodes de cada cluster
				for idcluster in $(sed '/^$/d' $nombre_archivo_ids_clusters);
                                do
                                        echo "Esperando por los datanodes del cluster $idcluster"
					ssh -A hadoop@"$(aws emr describe-cluster --cluster-id $idcluster | grep MasterPublicDnsName  | cut -d\" -f4)" -i ~/.ssh/keypair-amazon.pem 'bash -s' < /usr/local/hadoop/aws/cluster-de-clusters/wait-datanodes.sh "$slaves";
				done

				#Como todos los datanodes estan activos, podemos configurar los clusters en su totalidad
				for idcluster in $(sed '/^$/d' $nombre_archivo_ids_clusters);
                                do
                                        echo "Configurando en forma integral el cluster: $idcluster"
                                        ssh -A hadoop@"$(aws emr describe-cluster --cluster-id $idcluster | grep MasterPublicDnsName  | cut -d\" -f4)" -i ~/.ssh/keypair-amazon.pem 'bash -s' < /usr/local/hadoop/aws/iniciar-cluster-desde-master.sh "$2";
                                done

			else
				echo "Problemas...La cantidad de clusters requeridos no coinciden con la cantidad de archivos"
			fi
			
		else
			echo "El directorio no existe en el bucket, por favor proceda a su creacion"
		fi

	fi
fi
