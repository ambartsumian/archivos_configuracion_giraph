#!/bin/bash
if [ "$#" -ne 1 ]; then
        echo "Parametros: directorio-para-guardar-resultados-por-cluster"
else
	if aws s3 ls bucket-jose/giraphlogs/ | grep "$1"; then
        	echo "El directorio especificado como parametro existe en el bucket, se procedera a guardar los logs"
                echo "..."
	
    		ip_master="$(sudo -s ifconfig | grep -A 1 'eth0' | tail -1 | cut -d ':' -f 2 | cut -d ' ' -f 1)"
        	echo Ip del master: "$ip_master"
	
        	ips="$(/home/hadoop/bin/hdfs dfsadmin -report | grep Name | cut -f2 -d: | cut -f2 -d' ')"
        	#ips="$(/usr/local/hadoop/bin/hdfs dfsadmin -report | grep Name | cut -f2 -d: | cut -f2 -d' ')"
	
        	x=$(echo -e "$ips")

        	#Copiamos los caminos navegacionales encontrados al disco para despues copiarlos a S3
        	mkdir /tmp/resultados
        	mkdir /tmp/resultados/hdfs
        	mkdir /tmp/resultados/zookeeper
        	mkdir /tmp/resultados/yarnapps
		echo "Copiando resultados hdfs, limpiando lineas en blanco de los archivos"

		#Copiamos a una carpeta local los resultados de caminos navegacionales del cluster
        	hdfs dfs -copyToLocal hdfs:///user/hduser/output/caminosNavegacionales /tmp/resultados/hdfs

		#Iteramos uno por uno los archivos limpiando su contenido de lineas vacias
		SEARCH_FOLDER="/tmp/resultados/hdfs/caminosNavegacionales/*"
		IFS='%'
		for f in $SEARCH_FOLDER
		do
    		if [ -d "$f" ]
    		then
        		for ff in $f/*
        		do      
				if [ ! -d "$ff" ]
				then
					echo "Carpeta $f"
					echo "		Archivo $ff"
					sed '/^$/d' $ff >> /tmp/resultados/hdfs/caminos-navegacionales-resultantes
				fi
        		done
    		fi
		done
	
        	#Recorremos los SLAVES copiando los archivos de ZOOKEEPER
		echo "Copiando log zookeeper Master..."
		#Copiamos el log de zookeeper del master
		cat zookeeper.out >> /tmp/resultados/zookeeper/zookeeper-quorum.out
	        num=1
		while read ip_slave
	        do
	          	echo Copiando log zookeeper slave: "$ip_slave"
			printf '\n' >> /tmp/resultados/zookeeper/zookeeper-quorum.out
			printf 'Slave "$num1" --------------------------' >> /tmp/resultados/zookeeper/zookeeper-quorum.out
			ssh hadoop@"$ip_slave" 'cat zookeeper.out' >> /tmp/resultados/zookeeper/zookeeper-quorum.out 
			printf '\n' >> /tmp/resultados/zookeeper/zookeeper-quorum.out
	                num=$((num+1))
	        done <<< "$x"

		#Recorremos todas las aplicaciones YARN, quedandonos con sus resultados
		echo "Los LOGS de cada APP Yarn fueron copiadas automaticamente al directorio especificado por parametros"
		echo "Copiando todos los resultados del cluster a S3"
	        aws s3 cp /tmp/resultados/hdfs/caminos-navegacionales-resultantes s3://bucket-jose/giraphlogs/"$1"/resultados-$(cat /mnt/var/lib/info/job-flow.json | grep jobFlowId | cut -f2 -d: | cut -f2 -d'"')/caminos-navegacionales-resultantes
	        aws s3 cp /tmp/resultados/zookeeper/zookeeper-quorum.out s3://bucket-jose/giraphlogs/"$1"/resultados-$(cat /mnt/var/lib/info/job-flow.json | grep jobFlowId | cut -f2 -d: | cut -f2 -d'"')/zookeeper-quorum.out
	
	else
		echo "ERROR:       NO existe el directorio en el bucket, proceda a crearlo para poder guardar los logs alli. Debería usar el mismo directorio que uso para crear el cluster de clusters..."
	fi
fi


