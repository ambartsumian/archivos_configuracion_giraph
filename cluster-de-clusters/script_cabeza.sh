#!/bin/bash
if [ "$#" -ne 1 ]; then
        echo "Parametros: Tiempo maximo que puede durar una aplicacion corriendo"
else
    	until [ "1" !=  "1" ];
        do
          	app_anterior=$(/home/hadoop/bin/yarn application -list | cut -d' ' -f1 | grep -e "application" | awk -F ' ' '{print $1}')
                app_nueva=$app_anterior
                tiempo=0
                echo "Inicia applicacion: $app_anterior";
                until [ "$app_anterior" != "$app_nueva" ] || [ "$tiempo" -gt "$1" ];
                do
			echo "Tiempo de ejecucion actual de $app_anterior es : $tiempo"
                        app_nueva=$(/home/hadoop/bin/yarn application -list | cut -d' ' -f1 | grep -e "application" | awk -F ' ' '{print $1}')
                        sleep 5;
                        tiempo=$((tiempo + 5))
                done;
                /home/hadoop/bin/yarn application -kill "$app_anterior"
                echo "Se termino $app_anterior O se hizo un kill, se vuelve a empezar";
                sleep 2;
        done
fi

