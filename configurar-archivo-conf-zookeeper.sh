#!/bin/bash
## Parametros:
# $1 = IP-Master
# $2 = IP-Propia
# $3 = Id -> myid

if [ "$#" -ne 3 ]; then
        echo "Parametros: ip_master ip_slave id_nodo_zookeeper"
else
    	ip_propia="$2"

        ips="$(/home/hadoop/bin/hdfs dfsadmin -report | grep Name | cut -f2 -d: | cut -f2 -d' ')"
        #ips="$(/usr/local/hadoop/bin/hdfs dfsadmin -report | grep Name | cut -f2 -d: | cut -f2 -d' ')"

        #Iniciando configuracion SSH para ZOOKEEPER
        x=$(echo -e "$ips")

        echo Configuracion zoo.cfg en "$ip_propia"
        num=1
	# Si ambos parametros son iguales, estamos configurando el zoo.cfg del master
        if [ "$1" = "$2" ]
        then
            	echo server.1=0.0.0.0:2888:3888 >> /home/hadoop/giraph/zookeeper/conf/zoo.cfg
                echo server.1=0.0.0.0:2888:3888
        else
            	echo server.1="$1":2888:3888 >> /home/hadoop/giraph/zookeeper/conf/zoo.cfg
                echo server.1="$1":2888:3888
        fi

	while read ip_slave
        do
          	num=$((num+1))
                if [ "$ip_propia" = "$ip_slave" ]
                then
                    	echo server."$num"=0.0.0.0:2888:3888  >> /home/hadoop/giraph/zookeeper/conf/zoo.cfg
                        echo server."$num"=0.0.0.0:2888:3888
                else
                    	echo server."$num"="$ip_slave":2888:3888  >> /home/hadoop/giraph/zookeeper/conf/zoo.cfg
                        echo server."$num"="$ip_slave":2888:3888
                fi
        done <<< "$x"

        printf "$3" > /home/hadoop/giraph/zookeeper/temp_propia/myid
fi
