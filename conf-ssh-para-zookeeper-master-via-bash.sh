#!/bin/bash
if [ "$#" -ne 1 ]; then
        echo "Parametros: ip_master"
else

    	ips="$(/home/hadoop/bin/hdfs dfsadmin -report | grep Name | cut -f2 -d: | cut -f2 -d' ')"
        #ips="$(/usr/local/hadoop/bin/hdfs dfsadmin -report | grep Name | cut -f2 -d: | cut -f2 -d' ')"

        ip_master="$1"

        cd ~/.ssh/
        printf '\n' | ssh-keygen -t rsa -P ""
        cp ~/.ssh/id_rsa.pub /tmp/id_rsa_master.pub
        cat /tmp/id_rsa_master.pub >> $HOME/.ssh/authorized_keys

        #Creo variable con todas las IPs
        x=$(echo -e "$ips")

        while read ip_slave
        do
          	echo Configurando slave con IP: "$ip_slave"
                echo Copiando certificado .pem de AWS a slaves
                scp -i /tmp/keypair-amazon.pem -o StrictHostKeyChecking=no /tmp/keypair-amazon.pem hadoop@"$ip_slave":/tmp
                echo Copiando certificado del master a los slaves
                scp -i /tmp/keypair-amazon.pem -o StrictHostKeyChecking=no /tmp/id_rsa_master.pub hadoop@"$ip_slave":/tmp
        done <<< "$x"

        #Iniciando configuracion SSH para ZOOKEEPER
        while read ip_slave
        do
          	echo Configurando slave con IP: "$ip_slave"
                ssh -i /tmp/keypair-amazon.pem hadoop@"$ip_slave" 'bash -s' < /tmp/conf-ssh-para-zookeeper-slave-via-bash.sh "$ip_master" "$ip_slave"
        done <<< "$x"

        #Finalizando configuracion SSH
        while read ip_slave
        do
          	echo Configurando slave con IP: "$ip_slave"
                ssh -i /tmp/keypair-amazon.pem hadoop@"$ip_slave" 'bash -s' < /tmp/agregar-certificados-ssh-de-otros-nodos.sh "$ip_master" "$ip_slave"
                cat /tmp/"$ip_slave".pub >> ~/.ssh/authorized_keys
        done <<< "$x"

fi
