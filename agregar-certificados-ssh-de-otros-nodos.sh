#!/bin/bash
ips="$(/home/hadoop/bin/hdfs dfsadmin -report | grep Name | cut -f2 -d: | cut -f2 -d' ')"
#ips="$(/usr/local/hadoop/bin/hdfs dfsadmin -report | grep Name | cut -f2 -d: | cut -f2 -d' ')"

x=$(echo -e "$ips")

if [ "$#" -ne 2 ]; then
        echo "Parametros: ip_master ip_slave"
else
    	#Obtenemos IP del slave desde el master, dado que no se puede hacer ifconfig
        ip_propia="$2"

        #Finalizando configuracion SSH
        while read ip_slave
        do
          	if [ ! "$ip_propia" = "$ip_slave" ]
                then
                    	echo Configurando slave con IP: "$ip_slave"
                        cat /tmp/"$ip_slave".pub >> ~/.ssh/authorized_keys
                fi
        done <<< "$x"
fi
