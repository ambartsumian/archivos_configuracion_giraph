#!/bin/bash
if [ "$#" -ne 1 ]; then
        echo "Parametros: URL-con-xml-a-parsear"
else
	echo "Iniciando construccion de grafo de entrada para Giraph"
	'wget "$1" /tmp/dump-wikipedia'
	bzip2 -d enwikiquote-20160601-pages-articles-multistream.xml.bz2
	if [ ! -f /tmp/dump-wikipedia ];
        then
                echo "ERROR:                     No se encontro el DUMP"
        else
		echo "El dump de wiki fue correctamente descargado..."
	fi
fi
