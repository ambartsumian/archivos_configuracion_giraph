##Parametros:
# $1 = directorio-para-guardar-logs
# $2 = IP-Slave
if [ "$#" -ne 1 ]; then
        echo "Parametros: directorio-para-guardar-logs"
else

	#######   GIRAPH     #####
	mkdir /home/hadoop/giraph
	
	#Copiamos el jar de Giraph para que este disponible en los lugares de donde hadoop chupa los jars
	sudo -s aws s3 cp s3://bucket-jose/giraph-examples-1.1.0-for-hadoop-2.4.0-jar-with-dependencies.jar /home/hadoop/giraph/giraph.jar
	#sudo -s cp /home/hadoop/giraph/giraph.jar /home/hadoop/share/hadoop/mapreduce/giraph.jar
	sudo -s cp /home/hadoop/giraph/giraph.jar /home/hadoop/share/hadoop/yarn/lib/giraph.jar
	sudo -s aws s3 cp s3://bucket-jose/giraph/procesamiento-grafo-0.0.1.jar /home/hadoop/share/hadoop/yarn/lib/procesamiento-grafo-0.0.1.jar
	
	###### ZOOKEEPER    #######
	aws s3 cp s3://bucket-jose/giraph/zookeeper.tar.gz /home/hadoop/giraph/zookeeper.tar.gz
	tar xzf /home/hadoop/giraph/zookeeper.tar.gz -C /home/hadoop/giraph/
	mv /home/hadoop/giraph/zookeeper-aws /home/hadoop/giraph/zookeeper
	mkdir /home/hadoop/giraph/zookeeper/temp_propia
	
	mkdir /mnt1/zookeeper-disco-alternativo
	
	#Este paso es necesario dado que no sabemos en que maquina se ejecutra el GiraphManager, por ende dejamnos el archivo de vertices en todas
	aws s3 cp s3://bucket-jose/giraphlogs/"$1"/vertices-$(cat /mnt/var/lib/info/job-flow.json | grep jobFlowId | cut -f2 -d: | cut -f2 -d'"').txt /tmp/vertices.txt
	#El archivo en vertices.txt debe cargarse unicamente para pruebas puntuales, en la mayoria de los casos habra un archivo de estos por cluster
	#aws s3 cp s3://bucket-jose/giraph/datos/vertices.txt /tmp/vertices.txt
fi	
