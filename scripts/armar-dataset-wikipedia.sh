#!/bin/sh
if [ "$#" -lt 1 ]; then
        echo "Error, revise los parametros --> Parametros: archivo-con-consulta-sparql1 archivo-con-consulta-sparql2 ..."
else
	echo "Parametros recibidos correctamente. Procesando consultas sparql..."

	cat /dev/null > resultado-consultas-sparql-actualizado
	cat /dev/null > resultado-consultas-sparql-ordenado
	cat /dev/null > resultado-consultas-sparql-final
	for var in "$@"
	do
		echo "Procesando archivo: $var ..."
	   tail -n +2 "$var" | sed -E 's/\",\"/,/' | sed -E 's/\"//' | sed -E 's/\"//' | sed -E 's/\"//' | sed -E 's/\"//' | sed -e 's/http:\/\/es.dbpedia.org\/resource\///' -e 's/http:\/\/es.dbpedia.org\/resource\///' | sed -e 's/http:\/\/dbpedia.org\/resource\///' -e 's/http:\/\/dbpedia.org\/resource\///' | sed -E 's/,/-@-/' | sed -E 's/_/ /g' | sort | head -n 1000 >> resultado-consultas-sparql-actualizado
	done
	sort resultado-consultas-sparql-actualizado > resultado-consultas-sparql-ordenado
	uniq resultado-consultas-sparql-ordenado >  resultado-consultas-sparql-final
	rm resultado-consultas-sparql-actualizado
	rm resultado-consultas-sparql-ordenado

fi
echo "Procesamiento finalizado"
