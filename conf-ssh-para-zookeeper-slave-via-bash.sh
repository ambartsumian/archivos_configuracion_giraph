#!/bin/bash
##Parametros:
# $1 = IP-Master
# $2 = IP-Slave
if [ "$#" -ne 2 ]; then
        echo "Parametros: ip_master ip_slave"
else
    	ips="$(/home/hadoop/bin/hdfs dfsadmin -report | grep Name | cut -f2 -d: | cut -f2 -d' ')"

        #Creo variable con todas las IPs
        x=$(echo -e "$ips")

        #Obtenemos IP del slave desde el master, dado que no se puede hacer ifconfig
        ip_propia="$2"

        cd ~/.ssh/
        printf '\n' | ssh-keygen -t rsa -P ""

        echo Copiando .pub de "$ip_propia" a /tmp
        cp ~/.ssh/id_rsa.pub /tmp/"$ip_propia".pub
        cat /tmp/"$ip_propia".pub >> ~/.ssh/authorized_keys
        cat /tmp/id_rsa_master.pub >>  ~/.ssh/authorized_keys

        echo Copiando .pub de "$ip_propia" al MASTER
        scp -i /tmp/keypair-amazon.pem -o StrictHostKeyChecking=no /tmp/"$ip_propia".pub hadoop@"$1":/tmp
        while read ip_slave
        do
          	if [ ! "$ip_propia" = "$ip_slave" ]
                then
                    	echo Copiando .pub de "$ip_propia" a slave con IP: "$ip_slave"
                        scp -i /tmp/keypair-amazon.pem -o StrictHostKeyChecking=no /tmp/"$ip_propia".pub hadoop@"$ip_slave":/tmp
                fi
        done <<< "$x"
fi
